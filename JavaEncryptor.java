package com.calleAgProject.se;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

public class JavaEncryptor
{	
	public static HashMap<String, String> mToCipherToAlphabet = new HashMap<>();
	public static HashMap<String, String> mFromCipherToAlphabet = new HashMap<>();
	public static HashSet<Character> mLatterSigns = new HashSet<>();
	public static HashSet<Character> mFormerSigns = new  HashSet<>();
	public static HashSet<Integer> mForbiddenNumbers = new HashSet<>();
	public static int mSeed;
	
	public static void main(String[] tArgs)
	{
		Cipher();
	}

	public static void Cipher()
	{
		while (true)
		{
			String[] tOptions = {"Key word-based", "Seed-based"};							
			//Window with customizable buttons bellow
			int tVal = JOptionPane.showOptionDialog(new JFrame("Calle"),							//The theme (Don't know how it works)
					"What type of cipher do you want to use?",		//What the text says
					"Cipher to use",								//What the ... says
					0,												//No idea
					JOptionPane.QUESTION_MESSAGE,					//What type of dialog it is
					null,											//The icon
					tOptions,										//What the buttons says in an array
					tOptions[0]);									//The default selected button
			if (tVal == 0)
			{
				KeyWordBased();
			}
			else if (tVal == 1)
			{
				SeedBased();
			}
			int tExitQuestion = JOptionPane.showConfirmDialog(null, "Do you want to exit?");
			
			if (tExitQuestion == JOptionPane.YES_OPTION)
			{
				System.exit(0);
			}
		}
	}

	public static void GenerateSeedAlphabet()
	{
		String[] tAlphabet = {" ", ".", ",", "!", "?", "a", "b", "c", "d", "e", 
							  "f", "g", "h", "i", "j", "k", "l", "m", "n", "o",
							  "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", 
							  "z", "A", "B", "C", "D", "E", "F", "G", "H", "I",
							  "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", 
							  "T","U", "V", "X", "Y", "Z"}; //tAlphabet[55] == Z
		Random tRandomGenerator = new Random(mSeed);
		String tBufferForAcceptedSign = null;
		char tBufferForConvertingToChar = 0;
		int tSignCount = 0;
		
		mForbiddenNumbers.add(127);
		mForbiddenNumbers.add(129);
		mForbiddenNumbers.add(141);
		mForbiddenNumbers.add(143);
		mForbiddenNumbers.add(144);
		mForbiddenNumbers.add(157);
		mForbiddenNumbers.add(160);
		mForbiddenNumbers.add(173);
		mForbiddenNumbers.add(256);
		
		while (tSignCount <= 55)
		{
			int tBuffer = tRandomGenerator.nextInt(256);
			char tCharBuffer = (char) tBuffer;
			
			if (tBuffer < 32)
			{
				continue;
			}
			else if (mForbiddenNumbers.contains(tBuffer))
			{
				continue;
			}
			else if ((tBufferForAcceptedSign == null && mLatterSigns.contains(tCharBuffer)) || 
					 (tBufferForAcceptedSign != null && mFormerSigns.contains(tCharBuffer)))
			{
				continue;
			}
			
			if (tBufferForAcceptedSign == null)
			{
				tBufferForConvertingToChar = (char) tBuffer;
				tBufferForAcceptedSign = String.valueOf(tBufferForConvertingToChar);
				continue;
			}
			else
			{
				if (tBufferForConvertingToChar != (char) tBuffer)
				{	
					mFormerSigns.add(tBufferForConvertingToChar);
					mLatterSigns.add((char) tBuffer);
					tBufferForConvertingToChar = 0;
					tBufferForAcceptedSign += (char) tBuffer;
					mToCipherToAlphabet.put(tAlphabet[tSignCount], tBufferForAcceptedSign);
					mFromCipherToAlphabet.put(tBufferForAcceptedSign, tAlphabet[tSignCount]);
					tSignCount++;
				}
			}
		}
	}

	private static void KeyWordBased() 
	{
		JOptionPane.showMessageDialog(null, "Yet to be implemented  :(");
	}

	private static void SeedBased() 
	{
		int tHasSeed = JOptionPane.showConfirmDialog(null, "Du you have a 8-digit seed?"); // Yes/No window

		if (tHasSeed == JOptionPane.NO_OPTION)
		{
			int tUpper = 99999999;
			int tLower = 10000000;
			
			mSeed = (int) ((Math.random() * (tUpper - tLower)) + tLower);
			
			JOptionPane.showMessageDialog(null, "This text's seed is " + mSeed + 
												". Write it down! It will be needed to decipher the ciphered text!");
		}
		else if (tHasSeed == JOptionPane.YES_OPTION)
		{
			String tSeedAsString = JOptionPane.showInputDialog("What is your seed?");
			mSeed = Integer.parseInt(tSeedAsString);
		}
		
		GenerateSeedAlphabet();
//		System.exit(0); //Debug line to prevent the program to continue. Shall be commented out before final compilation.
	}

}
